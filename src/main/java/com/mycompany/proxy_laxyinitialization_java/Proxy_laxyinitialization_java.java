package com.mycompany.proxy_laxyinitialization_java;
// Image interface
interface Image {
    void display();
}
// RealImage class
class RealImage implements Image {
    private final String filename;
    public RealImage(String filename) {
        this.filename = filename;
        loadImageFromDisk();
    }
    private void loadImageFromDisk() {
        System.out.println("Loading image from disk: " + filename);
    }
    public void display() {
        System.out.println("Displaying image: " + filename);
    }
}
// ProxyImage class
class ProxyImage implements Image {
    private RealImage realImage;
    private final String filename;
    public ProxyImage(String filename) {
        this.filename = filename;
    }
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(filename);
        }
        realImage.display();
    }
}
public class Proxy_laxyinitialization_java {
    public static void main(String[] args) {
        Image image1 = new ProxyImage("image1.jpg");
        Image image2 = new ProxyImage("image2.jpg");
        image1.display(); // This will load and display "image1.jpg"
        image2.display(); // This will load and display "image2.jpg"
        image1.display(); //this won't load it will only display
    }
}
